module.exports = (ctx, next) => {
	if (ctx.message) {
		if (ctx.message.text == "/menu") {
			ctx.scene.leave()
			ctx.scene.enter("HelloScene")
		}

		if (ctx.message.text == "/chat") {
			ctx.scene.leave()
			ctx.scene.enter("ManagerScene")
		}
	}

	next()
}