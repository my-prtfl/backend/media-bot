const { Client } = require("db/models")

module.exports = async (ctx, next) => {
	try {
		const exist = await Client.findOne({ where: { tg_id: `${ctx.from.id}` } })

		const data = {
			username: ctx.from.username,
			fullName: ctx.from.first_name + (ctx.from.last_name ? " " + ctx.from.last_name : ""),
			tg_id: ctx.from.id.toString(),
			closed: false
		}

		let user
		if (!exist) {
			user = await Client.create(data)
		} else {
			user = await exist.update(data)
		}

		if (user.scene && !ctx.session.user) {
			ctx.session.user = user
			ctx.scene.leave()
			ctx.scene.enter(user.scene)
			return
		}

		ctx.session.user = user

		next()
	} catch(err) {
		console.error(err)
		next(false)
	}
}