const data = {
	cost: {
		text: "💲 Узнать стоимость рекламы",
		callback_data: "PromotionScene"
	},
	stock: {
		text: "💰 Актуальные акции",
		callback_data: "StockScene"
	},
	stat: {
		text: "📊 Статистика канала",
		callback_data: "StatsScene"
	},
	coop: {
		text: "🤝 Предложить сотрудничество",
		callback_data: "CooperationScene"
	},
	man: {
		text: "✅ Связаться с менеджером/Заказать рекламу",
		callback_data: "ManagerScene"
	},
	back: {
		text: "📃 Главное меню",
		callback_data: "HelloScene"
	}
}

exports.list = (...keys) => {
	const btns = []

	if (keys.length) {
		for (let key of keys) {
			if (!!data[key]) btns.push([ data[key] ])
		}
	} else {
		for (let prop of Object.keys(data)) {
			if (prop !== "back")
				btns.push([
					data[prop]
				])
		}
	}

	return btns
}

exports.goto = (ctx, btn) => {
	const items = Object.keys(data).map(prop => data[prop])
	const scene = !!items.find(({ callback_data }) => callback_data == btn)

	if (scene) {
		ctx.scene.leave()
		return ctx.scene.enter(btn)
	} else {
		ctx.reply("Это действие невозможно")
	}
}

exports.listeners = (scene, btns) => {
	scene.on('callback_query', ctx => {
		const cb = ctx.update?.callback_query?.data

		const exist = !!btns.find(([{ callback_data }]) => callback_data == cb)
		
		if (exist)
			exports.goto(ctx, cb)
		
		ctx.answerCbQuery()
	})
}