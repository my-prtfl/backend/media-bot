const { Client, Chat, Message } = require("db/models"),
			axios = require("axios"),
			got = require("got")

exports.parseMessage = ctx => {
	const text = ctx.update.message.text || ctx.update.message.caption || "",
				entities = ctx.update.message.entities

	if (!entities) return text
	
	let result = text, extra = 0
	for (let params of entities) {
		let first_i = params.offset + extra,
				last_i = params.offset + params.length + extra

		if (result[last_i - 1] == "\n") last_i -= 1

		let openTag, closeTag

		switch (params.type) {
			case "text_link":
				openTag = `<a href="${params.url}" target="_blank">`
				closeTag = "</a>"
				break;
			case "bold":
				openTag = "<b>"
				closeTag = "</b>"
				break;
			case "italic":
				openTag = "<i>"
				closeTag = "</i>"
				break;
			case "underline":
				openTag = "<u>"
				closeTag = "</u>"
				break;
			case "strikethrough":
				openTag = "<s>"
				closeTag = "</s>"
				break;
			case "code":
				openTag = "<code>"
				closeTag = "</code>"
				break;
		}

		extra += openTag.length + closeTag.length

		result = result.substring(0, first_i) + 
							openTag +
							result.substring( first_i, last_i ) +
							closeTag +
							result.substring(last_i, result.length)
	}
	result = result.replace(/\n/g, "<br>")
	return result
}

exports.getUser = async ctx => {
	return await Client.findOne({ where: { tg_id: ctx.from.id.toString() } })
}

exports.updateUser = async (ctx, updates) => {
	try {
		if (ctx.session?.user) {
			const user = await Client.findOne({ where: { id: ctx.session.user.id } })
			ctx.session.user = await user.update(updates)
		}
	} catch(err) {
		console.error(err)
	}
}

exports.getChat = async ctx => {
	try {
		const user = await Client.findOne({ where: { tg_id: ctx.from.id.toString() } })

		let chat = await user.getChat()
		if (!chat) {
			chat = await Chat.create()
			await user.setChat(chat)
		}

		return chat
	} catch(err) {
		console.error(err)
	}
}

exports.parseFile = async (ctx, type = "photo", updated = false) => {
	const prop = updated ? "edited_message" : "message"

	if (ctx.update[prop].photo && ctx.update[prop].photo.length && type == "photo")
		return ctx.update[prop].photo[ctx.update[prop].photo.length - 1].file_id

	if (ctx.update[prop].document?.file_id && type == "file")
		return {
			mime_type: ctx.update[prop].document.mime_type,
			name: ctx.update[prop].document.file_name,
			id: ctx.update[prop].document.file_id
		}
}

exports.createMessage = async ctx => {
	try {
		const chat = await exports.getChat(ctx)
		const user = await exports.getUser(ctx)

		const content = exports.parseMessage(ctx)
		const photo = await exports.parseFile(ctx)
		const file = await exports.parseFile(ctx, "file")

		const message = await Message.create({
			tg_id: ctx.update.message.message_id.toString(),
			content,
			photo,
			file
		})

		await chat.addMessage(message)
		await user.addMessage(message)

		return message
	} catch(err) {
		console.error(err)
	}
}

exports.editMessage = async ctx => {
	try {
		const message = await Message.findOne({ where: { tg_id: ctx.update.edited_message.message_id.toString() } })

		const content = ctx.update.edited_message.text || ctx.update.edited_message.caption
		const photo = await exports.parseFile(ctx, "photo", true)
		const file = await exports.parseFile(ctx, "file", true)

		return await message.update({ 
			content,
			photo,
			file 
		})
	} catch(err) {
		console.error(err)
	}
}