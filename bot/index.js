const { Telegraf, Scenes, session } = require("telegraf"),
			fs = require("fs"),
			commandsMiddleware = require("./middleware/commands"),
			userMiddleware = require("./middleware/user")

const token = process.env.TOKEN
const bot = new Telegraf(token)

fs.readdir('bot/scenes', async (err, files) => {
	if (err) throw new Error(err)

	const stage = new Scenes.Stage(
		files.map(file => require(`./scenes/${file}`))
	)
	
	bot.use( session() )
	bot.use( stage.middleware() )
	bot.use( commandsMiddleware )
	bot.use( userMiddleware )

	// bot.telegram.sendMediaGroup(252920458, [
	// 	{
	// 		type: "photo",
	// 		media: "https://www.intercosmetology.ru/upload/iblock/1eb/DSA_PILING_WHITE_kopiya.jpg",
	// 		caption: "<b>Привет</b> цмо",
	// 		parse_mode: "HTML"
	// 	},
	// 	{
	// 		type: "photo",
	// 		media: "https://www.intercosmetology.ru/upload/iblock/1eb/DSA_PILING_WHITE_kopiya.jpg",
	// 		caption: "<b>Привет</b> цмо",
	// 		parse_mode: "HTML"
	// 	}
	// ])

	// bot.telegram.sendPhoto(252920458, "https://media-bot.media.roybots.ru/storage/images/5-bgtop.webp", {
	// 	caption: "<b>Привет</b> цмо",
	// 	parse_mode: "HTML"
	// }).catch(console.error)

	await bot.telegram.setMyCommands([
		{
			command: "menu",
			description: "Вернуться в главное меню"
		},
		{
			command: "chat",
			description: "Начать чат с менеджером"
		}
	]).catch(console.error)

	bot.start(async ctx => {
		ctx.scene.enter("HelloScene")
	})

	bot.on("message", ctx => {
		if ([ "/chat", "/menu" ].includes(ctx.message.text)) return
		
		const text = `
			<b>Ошибка, сообщение не доставлено!</b>\n\nЧтобы войти в чат с менеджером, перейдите в главное меню /menu и нажмите кнопку «Связаться с менеджером/Заказать рекламу» <b>ИЛИ</b> введите команду /chat.
		`

		ctx.reply(text, {
			parse_mode: "HTML"
		})
	})

	bot.launch()
});

module.exports = bot