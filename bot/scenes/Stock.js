const { Scenes, Markup } = require("telegraf"),
			buttons = require("bot/common/buttons"),
			{ updateUser } = require("bot/common/user")

const stockScene = new Scenes.BaseScene("StockScene")

const sceneButtons = buttons.list(
	"man", "back"
)

stockScene.enter(async ctx => {
	await updateUser(ctx, { scene: "StockScene" })

	// const text = `
	// 	<b>Канал @СryptoPizza_News проводит акцию!</b>\n\n(тут будут указаны сроки акции)\n\nВсем рекламодателям, обратившимся к нам, мы делаем <b>скидку на один рекламный пост в размере 50%</b>. Акция распространяется на все доступные форматы.\n\nЭто позволит вам протестировать наш канал на наличие вашей аудитории, а также проверить фидбэк от рекламного поста, что позволит в будущем запускать на нашем канале еще более эффективную рекламу.\n\nЧтобы получить скидку, свяжитесь с нашим менеджером с помощью кнопки ниже и напишите о том, что вы хотели бы получить скидку.
	// `

	const photo = "https://bot.media.roybots.ru/storage/images/banner_discount.png"

	// const text = `
	// 	<b>Бум! 💥 Канал СryptoPizza News проводит акцию!</b>\n\nВсе рекламодатели до 31 октября могут заказать рекламу со скидкой 50%. 🔥 Это позволит вам протестировать эффективность рекламы за минимальную цену.\n\nЗабронировать рекламу со скидкой можно на любую дату, даже после 31 октября.\n\nДля получения скидки свяжитесь с нашим менеджером по кнопке ниже. 👇 Он не кусается 😅<a href="${photo}"> </a>. 
	// `

	const text = `
		<b>Актуальные акции</b>\n\nНа данный момент актуальных акций не найдено. Чтобы получать первыми самые горячие предложения — просто оставайтесь подписанными на нашего бота!
	`

	const markup = {
		reply_markup: {
			inline_keyboard: sceneButtons
		},
		parse_mode: "HTML",
		disable_web_page_preview: false
	}

	try {
		await ctx.editMessageText(text, markup)
	} catch(err) {
		ctx.reply(text, markup)
	}
})

buttons.listeners(stockScene, sceneButtons)

module.exports = stockScene