const { Scenes } = require("telegraf"),
			{ updateUser, createMessage, editMessage } = require("bot/common/user"),
			io = require("server/io")

const managerScene = new Scenes.BaseScene("ManagerScene")


managerScene.enter(async ctx => {
	let updated = ctx.session.user?.scene == "ManagerScene"
	
	await updateUser(ctx, { scene: "ManagerScene" })

	const markup = {
		reply_markup: {},
		parse_mode: "HTML",
		disable_web_page_preview: true
	}

	const text = "<b>Вы вошли в чат с менеджером</b>\n\nЧтобы попасть в главное меню, введите команду /menu"

	try {
		ctx.editMessageText(text, markup)
	} catch(err) {
		ctx.reply(text, markup)
	}

	if (updated)
		ctx.reply("Похоже, что бот был перезагружен, ваше сообщение не было отправлено\n\nПожалуйста, отправьте его ещё раз")
})

managerScene.command("menu", ctx => {
	ctx.scene.leave()
	ctx.scene.enter("HelloScene")
})

managerScene.on("message", async ctx => {
	await createMessage(ctx)
	io.sockets.emit(`new_message`, ctx.from.id)
	setTimeout(() => {
		io.sockets.emit(
			"notify", 
			`<a href="/chats/${ctx.from.id}" class="has-text-white is-underlined">${ctx.from.first_name ? ctx.from.first_name + (ctx.from.last_name ? " " + ctx.from.last_name : "") : ctx.from.username}</a> прислал новое сообщение`
		)
	}, 1000)
})

managerScene.on("edited_message", async ctx => {
	await editMessage(ctx)
	io.sockets.emit(`new_message`, ctx.from.id)
	setTimeout(() => {
		io.sockets.emit(
			"notify", 
			`<a href="/chats/${ctx.from.id}" class="has-text-white is-underlined">${ctx.from.first_name ? ctx.from.first_name + (ctx.from.last_name ? " " + ctx.from.last_name : "") : ctx.from.username}</a> прислал новое сообщение`
		)
	}, 1000)
})

// managerScene.on("t")

module.exports = managerScene