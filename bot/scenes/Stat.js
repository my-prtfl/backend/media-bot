const { Scenes, Markup } = require("telegraf"),
			buttons = require("bot/common/buttons"),
			{ updateUser } = require("bot/common/user")

const statsScene = new Scenes.BaseScene("StatsScene")

const sceneButtons = buttons.list(
	"man", "back"
)

statsScene.enter(async ctx => {
	await updateUser(ctx, { scene: "StatsScene" })

	// const text = `
	// 	Охват одного поста на канале @СryptoPizza_News: <b>~ 5–8 тыс</b>.\n\nБолее подробная статистика представлена тут:\n– Telemetr (https://telemetr.me/@cryptopizza_news);\n– TgStat (https://tgstat.ru/channel/@cryptopizza_news).\n\nАктуальную внутреннюю статистику посетителей в Telegram предоставляем по запросу.	
	// `

	const text = `
		<b>Статистика канала СryptoPizza News</b>\n\nОхват одного поста на канале @СryptoPizza_News: ~ 5–7 тыс.\n\nБолее подробная статистика представлена тут:\n\n– <a href="https://telemetr.me/@cryptopizza_news">Telemetr</a>;\n– <a href="https://tgstat.ru/channel/@cryptopizza_news">TgStat</a>.\n\nАктуальную внутреннюю статистику от Telegram предоставляем по запросу. Для этого свяжитесь с нашим менеджером — и все получите быстро и в самом лучшем виде. 👇	
	`

	const markup = {
		reply_markup: {
			inline_keyboard: sceneButtons
		},
		parse_mode: "HTML",
		disable_web_page_preview: true
	}

	try {
		ctx.editMessageText(text, markup)
	} catch(err) {
		ctx.reply(text, markup)
	}
})

buttons.listeners(statsScene, sceneButtons)

module.exports = statsScene