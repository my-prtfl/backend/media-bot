const { Scenes, Markup } = require("telegraf"),
			buttons = require("bot/common/buttons"),
			{ updateUser } = require("bot/common/user")

const cooperationScene = new Scenes.BaseScene("CooperationScene")

const sceneButtons = buttons.list(
	"man", "back"
)

cooperationScene.enter(async ctx => {
	await updateUser(ctx, { scene: "CooperationScene" })

	const text = `
		<b>Предложить сотрудничество</b>\n\nЕсли у вас есть предложение о сотрудничестве, которое может быть нам интересно, просто заполните небольшую гугл-форму (это всего 1–2 минуты, а может, и того меньше): https://forms.gle/4jFXeP8zYeHYtXgw8.\n\nМы рассмотрим ваше предложение в самое ближайшее время и свяжемся с вами.\n\nТакже вы можете написать нашему менеджеру в Телеграм с помощью кнопки ниже. 👇
	`

	const markup = {
		reply_markup: {
			inline_keyboard: sceneButtons
		},
		parse_mode: "HTML",
		disable_web_page_preview: true
	}

	try {
		ctx.editMessageText(text, markup)
	} catch(err) {
		ctx.reply(text, markup)
	}
})

buttons.listeners(cooperationScene, sceneButtons)

module.exports = cooperationScene