const { Scenes, Markup } = require("telegraf"),
			buttons = require("bot/common/buttons"),
			{ updateUser } = require("bot/common/user")

const promotionScene = new Scenes.BaseScene("PromotionScene")

const sceneButtons = buttons.list(
	"stock", "stat", "man", "back"
)

promotionScene.enter(async ctx => {
	await updateUser(ctx, { scene: "PromotionScene" })

	// const text = `
	// 	Стоимость рекламного поста в Telegram-канале @CryptoPizza_News:\n\n– 1 ч. в топе / 24 ч. в ленте: 500$;\n– 1 ч. в топе / 48 ч. в ленте: 600$;\n– 1 ч. в топе / без удаления: 700$.\n\n<b>+ дополнительный 1 ч. к топу: 200$.</b>\n\nВсе рекламные посты помечаются тегом #реклама.\n\n${ctx.from.first_name || ctx.from.username}, прямо сейчас у вас есть возможность получить скидку на рекламу в размере 50%. Для этого жмите на кнопку «Получить скидку на рекламу в размере 50%».	
	// `

	// const text = `
	// 	<b>Стоимость рекламы</b>\n\nСтоимость рекламного поста в Telegram-канале @CryptoPizza_News:\n\n🔸 2 ч. в топе/72 ч. в ленте: <s>$500</s> 250$.\n🔸 2 ч. в топе/без удаления: <s>$600</s> 300$.\n\n+ 1 дополнительный час к топу: <s>$150</s> 75$.\n\n<b>${ctx.from.first_name || ctx.from.username}, прямо сейчас у вас есть возможность купить рекламу в нашем канале по самой горячей цене и получить скидку в размере 50% без каких-либо условий. 🔥  Чтобы узнать подробности, жмите кнопку ниже «Актуальные акции». 👇</b>\n\nВсе рекламные посты помечаются тегом #реклама. Если вы хотите пост без данного тега — пишите, обсудим. Возможно, мы сможем сделать пост без хештега или даже нативный пост, но стоить он будет дороже. Также уточним, что скамы мы НЕ рекламируем ни за какие деньги.
	// `

	const text = `
		<b>Стоимость рекламы</b>\n\nСтоимость рекламного поста в Telegram-канале @CryptoPizza_News:\n\n🔸 1 ч. в топе, 72 ч. в ленте: <b>$200</b>.\n🔸 2 ч. в топе, 72 ч. в ленте: <b>$300</b>.\n\nДополнительные услуги:\n\n🔸 Дополнительный 1 ч. к топу: <b>$100</b>.\n🔸 Закреп на 1 ч. после топа: <b>$50</b>.\n\n<b>VIP формат:</b>\n\n🔝 3 ч. в топе, без удаления, закреп на 2 ч. после топа: <b>$400</b>.\n\nВсе рекламные посты помечаются тегом #реклама. Если вы хотите пост без данного тега — пишите, обсудим. Возможно, мы сможем сделать пост без хештега или даже нативный пост, но стоить он будет дороже. Также уточним, что скамы мы НЕ рекламируем ни за какие деньги.
	`

	const markup = {
		reply_markup: {
			inline_keyboard: sceneButtons
		},
		parse_mode: "HTML",
		disable_web_page_preview: true
	}

	try {
		ctx.editMessageText(text, markup)
	} catch(err) {
		ctx.reply(text, markup)
	}
})

buttons.listeners(promotionScene, sceneButtons)

module.exports = promotionScene