const { Scenes, Markup } = require("telegraf"),
			buttons = require("bot/common/buttons"),
			{ updateUser } = require("bot/common/user")

const helloScene = new Scenes.BaseScene("HelloScene")

const sceneButtons = buttons.list()

helloScene.enter(async ctx => {
	await updateUser(ctx, { scene: "HelloScene" })

	const markup = Markup
		.inlineKeyboard( sceneButtons )
		.resize(true)

	const text = "Выберите раздел"

	try {
		ctx.editMessageText(text, markup)
	} catch(err) {
		ctx.reply(text, markup)
	}
})

buttons.listeners(helloScene, sceneButtons)

// helloScene.action()

module.exports = helloScene