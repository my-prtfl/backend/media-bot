const socket = require("socket.io"),
			cookieParser = require("socket.io-cookie-parser"),
			{ server } = require("./app"),
			checkAuthorization = require("./common/check_authorization"),
			{ User } = require("db/models")

const startPooling = () => {
	const io = new socket.Server(server, { 
		cors: require("./common/cors")
	})

	io.use( cookieParser() )

	io.on("connection", async socket => {
		console.log(`${socket.id} connected`)

		const isAuthorized = await checkAuthorization(socket.request.cookies?.auth)
		if (!isAuthorized)
			socket.emit("401", "Unauthorized")

		socket.on("uid", async id => {
			const user = await User.findByPk(id)

			if (!user)
				return socket.emit("401", "Unauthorized")

			await user.update({ socket: socket.id })
		})

		socket.on("close", socket.disconnect)
		socket.conn.on("close", () => {
			console.log(`${socket.id} disconnected`)
		})
	})

	return io
}

const io = startPooling()

module.exports = io