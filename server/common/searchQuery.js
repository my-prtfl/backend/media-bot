const { Op } = require("sequelize")

module.exports = ( fields, value ) => {
	const data = {
		[Op.or]: []
	}
	
	for (let field of fields) {
		data[Op.or].push(
			{[field]: {[Op.substring]: value}},
			{[field]: {[Op.substring]: value.toUpperCase()}},
			{[field]: {[Op.substring]: `${value.charAt(0).toUpperCase()}${value.slice(1)}`}},
			{[field]: {[Op.startsWith]: `${value.charAt(0).toUpperCase()}${value.slice(1)}`}},
			{[field]: {[Op.startsWith]: `${value.toUpperCase()}`}},
			{[field]: {[Op.endsWith]: value}}
		)
	}

	return data
}