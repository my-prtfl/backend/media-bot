const origins = process.env.NODE_ENV !== 'production' ? [ "http://localhost:8100" ] : [ "https://bot-crm.media.bri4ka.ru" ]

module.exports = {
	origin: (o, cb) => {
		if (!o) return cb(null, true)
		return origins.includes(o) ? cb(null, true) : cb("Not allowed")
	},
	credentials: true,
	methods: "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
	exposedHeaders: 'Set-Cookie',
	allowedHeaders: 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept',
	preflightContinue: false
}