const searchQuery = require("./searchQuery")

module.exports = async (req, Model, include = [], order = []) => {
	const { page = 1, limit = 20, filter, filter_type } = req.query

	const params = {
		where: {}
	}
	
	if (filter_type && filter) {
		params.where = {
			...params.where,
			...searchQuery(filter_type.split(","), filter)
		}
	}

	const data = await Model.findAll({ 
		...params,
		offset: (page - 1) * limit,
		order: [
			...order,
			[ "id", "ASC" ]
		],
		limit,
		include
	})

	const count = await Model.count(params)
	const last_page = Math.ceil(count/limit) || 1

	return { data, last_page }
}