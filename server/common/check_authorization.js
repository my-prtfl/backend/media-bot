const { User } = require("db/models")

module.exports = async token => {
	if (!token) return false

	const user = await User.findOne({ where: { token } })

	if (!user) return false

	return user
}