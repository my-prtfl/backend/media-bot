const { Router } = require("express"),
			{ Group, Campagin, Client, Message, User, File } = require("db/models"),
			getList = require("server/common/getList"),
			io = require("server/io"),
			{ getChat } = require("bot/common/user"),
			bot = require("bot"),
			multer = require("server/storage/multer"),
			upload = require("server/storage/upload")

const replaceExt = str => str.substr(0, str.lastIndexOf("."))

const router = Router()

router.route("/")
	// Получаем список рассылок
	.get(async (req, res) => {
		try {
			const { data: campagins, last_page } = await getList(req, Campagin, [{ model: Group, as: "group" }])

			res.send({ campagins, last_page })
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Создаём новую рассылку
	.post(multer, async (req, res) => {
		try {
			const { group: groupId, message } = req.body,
						{ files } = req

			if (!message)
				return res
					.status(400)
					.send({ message: "Тест рассылки обязателен для заполнения" })

			if (!groupId)
				return res
					.status(400)
					.send({ message: "Нужно выбрать группу" })

			// Загружаем и создаём файлы в бд
			const isImage = val => [ "image/jpeg", "image/png", "image/webp" ].includes(val)

			const messageFiles = []
	
			for (let file of files) {
				const bucket = isImage(file.mimetype) ? "images" : "files"
	
				let db_file = await File.create({ bucket })
	
				let src
				if (bucket == "images")
					src = await upload(file, `${db_file.id}-${replaceExt(file.originalname)}`)
				else
					src = await upload(file, `${db_file.id}-${file.originalname}`, bucket)
	
				db_file = await db_file.update({ src })
	
				messageFiles.push(db_file)
			}

			const group = await Group.findByPk(groupId) 

			if (!group)
				return res
					.status(400)
					.send({ message: "Группы с таким id не существует" })

			const campagin = await Campagin.create({ message })
			await campagin.setFiles( messageFiles )

			await campagin.setGroup(group)

			res.send(`${campagin.id}`)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

router.route("/:id([0-9]{1,})")
	// Получаем конкретную рассылку по id
	.get(async (req, res) => {
		try {
			const { id } = req.params

			const campagin = await Campagin.findOne({ 
				where: { id },
				include: [
					{
						model: Group,
						as: "group",
						include: [{
							model: Client,
							as: "clients"
						}]
					},
					{
						model: File,
						as: "files"
					}
				]
			})

			if (!campagin)
				return res
					.status(404)
					.send({ message: "Рассылка с таким id не найдена" })

			res.send(campagin)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Удаляем рассылку
	.delete(async (req, res) => {
		try {
			const { id } = req.params

			const campagin = await Campagin.findByPk(id)

			if (!campagin)
				return res
					.status(404)
					.send({ message: "Рассылка с таким id не найдена" })

			if (campagin.status !== "created")
				return res
					.status(400)
					.send({ message: "Нельзя удалять уже начавшиеся рассылки" })

			await campagin.destroy()

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	
router.route("/:id([0-9]{1,})/start")
	// Начать рассылку
	.post(async (req, res) => {
		try {
			const { id } = req.params

			const campagin = await Campagin.findOne({
				where: { id },
				include: [
					{
						model: Group,
						as: "group",
						include: [{
							model: Client,
							as: "clients"
						}]
					},
					{
						model: File,
						as: "files"
					}
				]
			})

			if (!campagin)
				return res
					.status(404)
					.send({ message: "Группа с таким id не найдена" })

			if (campagin.status == "sended")
				return res.send({ message: "Эта рассылка уже отправлена" })

			const clients = campagin.group.clients.filter(client => !campagin.sended.find(id => client.id == id))

			await campagin.update({ status: "pending" })
			
			io.emit("update_campagins")
			res.sendStatus(200)

			const manager = await User.findByPk(res.locals.user.id)

			let sended = campagin.sended
			let blocked = campagin.blocked

			let docs = null
			let images = null

			if (campagin.files.length) {
				docs = campagin.files.filter(({ bucket }) => bucket == "files")
				images = campagin.files.filter(({ bucket }) => bucket == "images")
			}

			for (let client of clients) {
				let content = campagin.message
												.replace(/<p>/g, "")
												.replace(/<\/p>/g, "\n")
												.replace(/<br>/, "\n")

				if (docs || images) {
					for (let image of images) {
						await bot.telegram.sendPhoto(client.tg_id, `https://media-bot.media.roybots.ru/storage/${image.bucket}/${image.src}`)
						await new Promise(resolve => setTimeout(() => resolve(true), 300))
					}
		
					for (let doc of docs) {
						await bot.telegram.sendDocument(client.tg_id, `https://media-bot.media.roybots.ru/storage/${doc.bucket}/${doc.src}`)
						await new Promise(resolve => setTimeout(() => resolve(true), 300))
					}
				}

				await bot.telegram.sendMessage(client.tg_id, content, {
					parse_mode: "HTML",
					disable_web_page_preview: true
				}).catch(() => blocked++)

				sended.push(client.id)
				campagin.sended = null
				await campagin.save()
				campagin.sended = sended
				await campagin.save()

				await getChat({ from: { id: client.tg_id } })

				const user = await Client.findByPk(client.id)
				const chat = await user.getChat()
				const message = await Message.create({ content })

				if (campagin.files.length)
					await Promise.all(
						campagin.files.map(({ id }) => message.addExtra(id))
					)

				await manager.addMessage(message)
				await chat?.addMessage(message)
				
				await campagin.update({ blocked })
				const checkout = await Campagin.findByPk(id)
				if (checkout.status == "stoped") break;
				
				await new Promise(resolve => setTimeout(() => resolve(true), 300))

				io.sockets.emit(`new_message`, client.tg_id)
				io.emit(`progress_${id}`, { sended, blocked })
			}

			let checkout = await Campagin.findByPk(id)

			if (checkout.status !== "stoped") {
				await campagin.update({ status: "sended" })
				io.emit(`success_${id}`)
			}

			io.emit("update_campagins")

		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

router.route("/:id([0-9]{1,})/stop")
	// Останавливаем рассылку
	.post(async (req, res) => {
		try {
			const { id } = req.params

			const campagin = await Campagin.findByPk(id)

			await campagin.update({ status: "stoped" })

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router