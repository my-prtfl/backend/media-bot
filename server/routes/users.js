const { Router } = require("express"),
			{ User } = require("db/models"),
			bcrypt = require("bcrypt"),
			{ Op } = require("sequelize")

const router = Router()

const checkPermissions = (req, res, next) => {
	if (!res.locals.user.root)
		return res.sendStatus(403)

	next()
}

router.use(checkPermissions)

router.route("/")
	// Получаем список существующих пользователей (кроме админа)
	.get(async (req, res) => {
		try {
			const users = await User.findAll({
				where: {
					username: {
						[Op.not]: "admin"
					}
				}
			})

			res.send(users)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Добавляем нового администратора
	.post(async (req, res) => {
		try {
			const { username, password, root } = req.body

			if (!username)
				return res
					.status(400)
					.send({ message: "Имя пользователя обязательно для заполнения" })

			if (!password)
				return res
					.status(400)
					.send({ message: "Пароль обязателен для заполнения" })

			const exist = await User.findOne({ where: { username } })
			// console.log(exist)

			if (!!exist)
				return res
					.status(400)
					.send({ message: "Пользователь с таким логином уже существует" })

			const saltRounds = Math.floor( Math.random() * 10 )
			const hash = await bcrypt.hash(password, saltRounds)

			await User.create({ username, hash, root })
			
			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	
router.route("/:id")
	// Удаляем пользователя
	.delete(async ( req, res ) => {
		try {
			const { id } = req.params

			const user = await User.findByPk(id)

			if (!user)
				return res.sendStatus(404)

			await user.destroy()

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router