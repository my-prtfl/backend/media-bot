const { Router } = require("express"),
			searchQuery = require("server/common/searchQuery"),
			{ Client, Message, Chat, User, Group, File } = require("db/models"),
			bot = require("bot"),
			db = require("db"),
			io = require("server/io"),
			dayjs = require("dayjs"),
			{ getChat } = require("bot/common/user"),
			getList = require("server/common/getList"),
			multer = require("server/storage/multer"),
			upload = require("server/storage/upload")

const router = Router()

const getDate = m => {
	if (m.uSender)
		return dayjs(m.createdAt).toDate()
	else
		return dayjs(m.updatedAt).toDate()
}

router.route("/")
	// Вытаскиваем клиентов из бд
	.get(async (req, res) => {
		try {
			const { groupName } = req.query

			const { page = 1, limit = 20, filter, filter_type } = req.query

			let query = `
				SELECT
					"Clients".*,
					array_agg(row_to_json("groups")) AS "groups",
					"messages"."count" as "messages"
				FROM "Clients"
				LEFT JOIN (
						SELECT
							"g"."id",
							"g"."name",
							"ug"."ClientId"
						FROM  "UserGroups" as "ug"
						LEFT JOIN "Groups" as "g"
							on "ug"."GroupId" = "g"."id"`

			query += `
				) AS "groups"
					ON "groups"."ClientId" = "Clients"."id"
				LEFT JOIN (
						SELECT
							"Messages"."clientId",
							COUNT(*) as "count"
						FROM "Messages"
						WHERE "Messages"."watched" = FALSE
						GROUP BY "clientId"
							
					) as "messages"
					ON "messages"."clientId" = "Clients"."id"`

			if ((filter_type && filter) || groupName)
					query += `
						WHERE
					`

			if (filter_type && filter)
					query += `
						("Clients"."username" ILIKE '%${filter}%'
						OR "Clients"."username" ILIKE '%${filter}'
						OR "Clients"."username" ILIKE '${filter}%'
						OR "Clients"."fullName" ILIKE '%${filter}%'
						OR "Clients"."fullName" ILIKE '%${filter}'
						OR "Clients"."fullName" ILIKE '${filter}%')
						${groupName ? "OR" : ""}
					`

			if (groupName)
				query += `
						("groups"."name" ILIKE '%${groupName}%'
						OR "groups"."name" ILIKE '%${groupName}'
						OR "groups"."name" ILIKE '${groupName}%')
				`

				query += `GROUP BY "Clients"."id", "messages"."count"
				ORDER BY "messages"."count", "Clients"."id"
				LIMIT ${limit}
				OFFSET ${(page - 1) * limit}
			`

			const [clients] = await db.query(query)
			const [[{ count }]] = await db.query(`SELECT COUNT(*) FROM "Clients"`)

			const last_page = Math.ceil(count/limit) || 1

			res.send({ clients, last_page })
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

router.route("/:tg_id")
	// Вытаскиваем из бд конкретного пользователя
	.get(async (req, res) => {
		try {
			const { tg_id } = req.params

			await getChat({ from: { id: tg_id.toString() } })

			const user = await Client.findOne({ 
				where: { tg_id: tg_id.toString() },
				include: [{
					model: Chat,
					as: "chat",
					include: [{
						model: Message,
						as: "messages",
						include: [
							{
								model: Client,
								as: "cSender"
							},
							{
								model: User,
								as: "uSender"
							},
							{
								model: File,
								as: "extras"
							}
						]
					}]
				}]
			})

			if (!user)
				return res.sendStatus(404)

			if (user.chat?.messages) {
				await Promise.all(
					user.chat.messages.map(message => !message.watched ? message.update({ watched: true }) : Promise.resolve(true))
				)

				user.chat.messages = user.chat.messages.sort((a, b) => getDate(a) - getDate(b))
			}

			res.send(user)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Добавляем/Убираем клиента из какой-либо группы
	.put(async (req, res) => {
		try {
			const { tg_id: client_id } = req.params,
						{ group_id, action } = req.body

			if (!group_id)
				return res
					.status(400)
					.send({ message: "ID группы является обязательным параметром" })

			if (!action)
				return res
					.status(400)
					.send({ message: "Не указано действие" })

			if (action !== "add" && action !== "remove")
				return res
					.status(400)
					.send({ message: "Действие указано не верно" })

			const group = await Group.findByPk(group_id)

			if (!group)
				return res
					.status(400)
					.send({ message: "Группа с таким ID не найдена" })

			const client = await Client.findByPk(client_id)

			if (!client)
				return res
					.status(404)
					.send({ message: "Пользователь с таким ID не найден" })

			await group[`${action}Client`](client_id)

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

const replaceExt = str => str.substr(0, str.lastIndexOf("."))

router.route("/:tg_id/send_message")
	// Отправляем сообщение пользователю
	.post(multer, async (req, res) => {
		const { tg_id } = req.params,
					{ files } = req

		const isImage = val => [ "image/jpeg", "image/png", "image/webp" ].includes(val)

		const messageFiles = []

		for (let file of files) {
			const bucket = isImage(file.mimetype) ? "images" : "files"

			let db_file = await File.create({ bucket })

			let src
			if (bucket == "images")
				src = await upload(file, `${db_file.id}-${replaceExt(file.originalname)}`)
			else
				src = await upload(file, `${db_file.id}-${file.originalname}`, bucket)

			db_file = await db_file.update({ src })

			messageFiles.push(db_file)
		}

		let content = req.body.content
										.replace(/<p>/g, "")
										.replace(/<\/p>/g, "\n")
										.replace(/<br>/, "\n")

		const user = await Client.findOne({ where: { tg_id: tg_id.toString() } })
		const manager = await User.findByPk(res.locals.user.id)
		const chat = await user.getChat()
		const message = await Message.create({ content })

		
		await manager.addMessage(message)
		await chat.addMessage(message)
		await Promise.all([
			messageFiles.map(file => message.addExtra(file))
		])
		
		if (user.scene !== "ManagerScene")
			content += `\nЧтобы ответить на это сообщение, войдите в чат с Менеджером: /chat`
		
		let body
		try {
			if (messageFiles.length) {
				const docs = messageFiles.filter(({ bucket }) => bucket == "files")
				const images = messageFiles.filter(({ bucket }) => bucket == "images")

				for (let image of images) {
					await bot.telegram.sendPhoto(user.tg_id, `https://media-bot.media.roybots.ru/storage/${image.bucket}/${image.src}`)
				}

				for (let doc of docs) {
					await bot.telegram.sendDocument(user.tg_id, `https://media-bot.media.roybots.ru/storage/${doc.bucket}/${doc.src}`)
				}
			}

			if (content)
				await bot.telegram.sendMessage(tg_id, content, {
					parse_mode: "HTML",
					disable_web_page_preview: true
				})
		} catch(err) {
			console.error(err)
			body = { message: "Сообщение не было отправлено" }
			await message.update({ sended: false })
		}

		io.sockets.emit(`new_message`, tg_id)

		if (body)
			return res.send(body)

		res.sendStatus(200)
	})
	// Повторная отпрака сообщения
	.put(async (req, res) => {
		const { tg_id } = req.params,
					{ message_id } = req.body
		
		const user = await Client.findOne({ where: { tg_id: tg_id.toString() } }) 
		const message = await Message.findOne({ 
			where: { id: message_id },
			include: [{
				model: File,
				as: "extras"
			}]
		})

		let content = message.content

		if (user.scene !== "ManagerScene")
			content += `\nЧтобы ответить на это сообщение, войдите в чат с Менеджером: /chat`

		let body
		try {
			if (message.extras.length) {
				const docs = message.extras.filter(({ bucket }) => bucket == "files")
				const images = message.extras.filter(({ bucket }) => bucket == "images")
	
				for (let image of images) {
					await bot.telegram.sendPhoto(user.tg_id, `https://media-bot.media.roybots.ru/storage/${image.bucket}/${image.src}`)
				}
	
				for (let doc of docs) {
					await bot.telegram.sendDocument(user.tg_id, `https://media-bot.media.roybots.ru/storage/${doc.bucket}/${doc.src}`)
				}
			}
	
			await bot.telegram.sendMessage(tg_id, content, {
				parse_mode: "HTML",
				disable_web_page_preview: true
			})

			io.sockets.emit(`new_message`, tg_id)
			await message.update({ sended: true })
		} catch(err) {
			console.error(err)
			body = { message: "Сообщение не было отправлено" }
			await message.update({ sended: false })
		}

		if (body)
			return res.send(body)

		res.sendStatus(200)
	})

module.exports = router