const { Router } = require("express"),
			{ User } = require("../sequelize/models"),
			bcrypt = require("bcrypt"),
			randToken = require("rand-token")

const router = Router()

router.route("/")
	// Авторизация по логину/паролю
	.post(async (req, res) => {
		try {
			const { username, password } = req.body

			if (!username || !password) {
				res.status(400)
				return res.send({ message: "Заполните все поля" })
			}
	
			const user = await User.findOne({ where: { username } })
			if (!user) {
				res.status(404)
				return res.send({ message: "Пользователь не найден" })
			}
	
			const valid =	await bcrypt.compare(password, user.hash)
			if (!valid) {
				res.status(401)
				res.clearCookie("auth")
				return res.send({ message: "Неверный пароль" })
			}

			const token = randToken.generate(8)

			res.cookie("auth", token, {
				maxAge: 7 * 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: process.env.NODE_ENV == "production"
			})

			user.token = token

			await user.save()

			delete user.hash
			delete user.token

			res.send(user)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Авторизация по токену
	.get(async (req, res) => {
		try {
			const { auth } = req.cookies

			if (!auth) return res.sendStatus(401)

			const user = await User.findOne({ where: { token: auth }, attributes: { exclude: ['token', 'hash' ] } })

			if (!user) {
				res.clearCookie('auth')
				return res.sendStatus(401)
			}

			res.send(user)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Выход из аккаунта
	.delete(async (req, res) => {
		try {
			const { auth } = req.cookies
			
			if (!auth) return res.sendStatus(400)
	
			const user = await User.findOne({ where: { token: auth } })

			user.token = null
			await user.save()

			res.clearCookie("auth")
			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router