const { Router } = require("express"),
			{ Group, Client } = require("db/models"),
			getList = require("server/common/getList"),
			{ Op } = require("sequelize")

const router = Router()

router.route("/")
	// Получаем список групп
	.get(async (req, res) => {
		try {
			const { data: groups, last_page } = await getList(req, Group, [{ model: Client, as: "clients" }])

			res.send({ groups, last_page })
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Создаём новую группу
	.post(async (req, res) => {
		try {
			const { name, clients } = req.body

			if (!name)
				return res
					.status(400)
					.send({ message: "Вы не заполнили название группы" })

			const group = await Group.create({ name })

			for (let id of clients) {
				const client = await Client.findByPk(id)
				await group.addClient(client)
			}

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})


router.route("/:id([0-9]{1,})")
	// Получаем конкретную группу по id
	.get(async (req, res) => {
		try {
			const { id } = req.params

			const group = await Group.findOne({
				where: { id },
				include: [{
					model: Client,
					as: "clients"
				}]
			})

			if (!group)
				return res
					.status(404)
					.send({ message: "Группы с таким id не существует" })

			res.send(group)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Редактируем группу
	.put(async (req, res) => {
		try {
			const { id } = req.params,
						{ name, clients } = req.body

			const group = await Group.findByPk(id)

			if (!group)
				res
					.status(404)
					.send({ message: "Группы с таким id не существует" })

			await group.update({ name })

			await group.setClients(
				await Promise.all(
					await Client.findAll({
						where: {
							id: {
								[Op.or]: clients
							}
						}
					})
				)
			) 

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Удаляем группу
	.delete(async (req, res) => {
		try {
			const { id } = req.params

			const group = await Group.findByPk(id)

			if (!group)
				return res
					.status(404)
					.send({ message: "Группы с таким id не существует" })

			await group.destroy()

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router