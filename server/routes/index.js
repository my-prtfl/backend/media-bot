const { Router } = require("express"),
			fs = require("fs"),
			checkAuthorization = require("../common/check_authorization")

const router = Router()

fs.readdirSync(__dirname).forEach(file => {
	if (file !== "index.js") {
		var name = file.replace(".js", "")

		if (name == "auth")
			router.use(`/${name}`, require(`./${file}`))
		else
			router.use(`/${name}`, async (req, res, next) => {
				try {
					const { auth } = req.cookies
					
					const isAuthorized = await checkAuthorization(auth)

					if (!isAuthorized)
						return res.sendStatus(401)

					res.locals.user = isAuthorized

					next()
				} catch(err) {
					console.error(err)
					return res.sendStatus(500)
				}
			}, require(`./${file}`))
	}
})

module.exports = router