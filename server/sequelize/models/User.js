const { Model, DataTypes } = require("sequelize")

module.exports = sequelize => {
	class User extends Model {}

	User.init(
		{
			username: {
				type: DataTypes.STRING,
				allowNull: false
			},
			hash: {
				type: DataTypes.STRING,
				allowNull: false
			},
			token: {
				type: DataTypes.STRING,
				allowNull: true
			},
			sockets: {
				type: DataTypes.ARRAY(DataTypes.STRING),
				defaultValue: []
			},
			root: {
				type: DataTypes.BOOLEAN,
				defaultValue: true
			}
		},
		{
			modelName: "User",
			timestamps: false,
			sequelize
		}
	)

	return User
}