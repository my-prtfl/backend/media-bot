const { Model, DataTypes } = require("sequelize")

module.exports = sequelize => {
	class Message extends Model {}

	Message.init(
		{
			content: {
				type: DataTypes.TEXT,
				allowNull: true
			},
			watched: {
				type: DataTypes.BOOLEAN,
				defaultValue: false
			},
			edited: {
				type: DataTypes.BOOLEAN,
				defaultValue: false
			},
			tg_id: {
				type: DataTypes.STRING,
				defaultValue: 0
			},
			file: {
				type: DataTypes.JSONB,
				/*
					{
						id: TEXT, // File id
						name: TEXT // File name
						mime_type: TEXT
					}
				*/
				allowNull: true
			},
			photo: {
				type: DataTypes.TEXT,
				allowNull: true
			},
			sended: {
				type: DataTypes.BOOLEAN,
				defaultValue: true
			}
		},
		{
			sequelize,
			modelName: "Message"
		}
	)

	return Message
}