const db = require("db")

exports.User = require("./User")(db)
exports.Client = require("./Client")(db)
exports.Group = require("./Group")(db)
exports.Message = require("./Message")(db)
exports.Chat = require("./Chat")(db)
exports.Campagin = require("./Campagin")(db)
exports.UserGroups = require("./UserGroups")(db)
exports.File = require("./File")(db)
exports.MessageFiles = require("./MessageFiles")(db)

/* Устанавливаем ассоциации в модельках */
db.models.Group.belongsToMany(db.models.Client, { as: "clients", through: db.models.UserGroups })
db.models.Client.belongsToMany(db.models.Group, { as: "groups", through: db.models.UserGroups })

db.models.Message.belongsTo(db.models.Client, { as: "cSender", foreignKey: "clientId" })
db.models.Client.hasMany(db.models.Message, { as: "messages", foreignKey: "clientId" })
db.models.Message.belongsTo(db.models.User, { as: "uSender", foreignKey: "userId" })
db.models.User.hasMany(db.models.Message, { as: "messages", foreignKey: "userId" })

db.models.Message.belongsToMany(db.models.File, { as: "extras", through: db.models.MessageFiles })
db.models.File.belongsToMany(db.models.Message, { as: "messages", through: db.models.MessageFiles })

db.models.Message.belongsTo(db.models.Chat, { as: "message", foreignKey: "chatId" })
db.models.Chat.hasMany(db.models.Message, { as: "messages", foreignKey: "chatId" })

db.models.Client.hasOne(db.models.Chat, { as: "chat", foreignKey: "clientId" })
db.models.Chat.belongsTo(db.models.Client, { as: "user", foreignKey: "clientId" })

db.models.Campagin.belongsTo(db.models.Group, { as: "group" })
db.models.Group.hasMany(db.models.Campagin, { as: "campagins" })

db.models.Campagin.hasMany(db.models.File, { as: "files", foreignKey: "campaginId" })
db.models.File.belongsTo(db.models.Campagin, { as: "campagin", foreignKey: "campaginId" })

db.models.Campagin.belongsTo(db.models.Message, { as: "campagin", foreignKey: "messageId" })
db.models.Message.hasOne(db.models.Campagin, { as: "campagin", foreignKey: "messageId" })

;(async () => {
	try {
		await db.sync({ alter: true })
		console.log("Models\x1b[32m",  Object.keys(db.models).map(item => item).join(', '), "\x1b[0mare in sync")
	} catch(err) {
		console.error(err)
	}
})();

// module.exports = db.models