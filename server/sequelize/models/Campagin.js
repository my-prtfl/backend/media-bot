const { Model, DataTypes } = require("sequelize")

module.exports = sequelize => {
	class Campagin extends Model {}

	Campagin.init(
		{
			status: {
				type: DataTypes.STRING,
				defaultValue: "created"
				/** 
				 * [
				 * 	"created",
				 * 	"pending",
				 * 	"stoped",
				 * 	"sended"
				 * ]
				*/
			},
			// Кол-во отправленных сообщений
			sended: {
				type: DataTypes.JSONB,
				defaultValue: []
			},
			// Кол-во пользователей с заблокированным ботом
			blocked: {
				type: DataTypes.INTEGER,
				defaultValue: 0
			},
			message: {
				type: DataTypes.TEXT,
				allowNull: false
			}
		},
		{
			modelName: "Campagin",
			createdAt: false,
			sequelize
		}
	)

	return Campagin
}