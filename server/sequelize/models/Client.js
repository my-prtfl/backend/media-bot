const { Model, DataTypes } = require("sequelize")

module.exports = sequelize => {
	class Client extends Model {}

	Client.init(
		{
			username: {
				type: DataTypes.STRING,
				allowNull: false
			},
			fullName: {
				type: DataTypes.STRING,
				allowNull: false
			},
			tg_id: {
				type: DataTypes.STRING,
				allowNull: false
			},
			closed: {
				type: DataTypes.BOOLEAN,
				defaultValue: false
			},
			scene: {
				type: DataTypes.STRING,
				allowNull: true
			}
		},
		{
			modelName: "Client",
			updatedAt: false,
			sequelize
		}
	)

	return Client
}