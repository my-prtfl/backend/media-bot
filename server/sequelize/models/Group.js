const { Model, DataTypes } = require("sequelize")

module.exports = sequelize => {
	class Group extends Model {}

	Group.init(
		{
			name: {
				type: DataTypes.STRING,
				allowNull: false
			}
		},
		{
			modelName: "Group",
			timestamps: false,
			sequelize
		}
	)

	return Group
}