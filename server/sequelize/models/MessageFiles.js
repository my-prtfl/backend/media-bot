const { Model, DataTypes } = require("sequelize")

module.exports = sequelize => {
	class MessageFiles extends Model {}

	MessageFiles.init(
		{},
		{
			modelName: "MessageFiles",
			timestamps: false,
			sequelize
		}
	)

	return MessageFiles
}