const { Model, DataTypes } = require("sequelize")

module.exports = sequelize => {
	class Chat extends Model {}

	Chat.init(
		{},
		{
			modelName: "Chat",
			timestamps: false,
			sequelize
		}
	)

	return Chat
}