const { DataTypes, Model } = require("sequelize")

module.exports = sequelize => {
	class File extends Model {}

	File.init({
		bucket: {
			type: DataTypes.TEXT,
			allowNull: false
		},
		src: {
			type: DataTypes.TEXT,
			allowNull: true
		}
	}, {
		modelName: "File",
		timestamps: false,
		sequelize
	})

	return File
}