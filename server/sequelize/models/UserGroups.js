const { Model, DataTypes } = require("sequelize")

module.exports = sequelize => {
	class UserGroups extends Model {}

	UserGroups.init(
		{},
		{
			modelName: "UserGroups",
			timestamps: false,
			sequelize
		}
	)

	return UserGroups
}