const { Sequelize } = require('sequelize')

const connection = new Sequelize(process.env.DB_URL, {
	dialect: 'postgres',
	protocol: 'postgres',
	dialectOptions: {},
	logging: false
})

connection.authenticate()
	.then(() => console.log("Successfully connected to database"))
	.catch(console.error)

module.exports = connection