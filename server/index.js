const { app, server } = require("./app.js")

require("./io")

app.use("/", require("./routes"))
			
const port = process.env.PORT || 8000
server.listen(port, () => console.log("Server started on port %s", port))