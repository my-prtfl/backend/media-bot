const express = require("express"),
			http = require("http"),
			{ json, urlencoded } = require("express"),
			helmet = require("helmet"),
			cookieParser = require("cookie-parser"),
			cors = require("cors")

const app = express()

app.use( require("server/no_cors_routes") )

app.use( cors(require("./common/cors")) )
app.use( cookieParser() )
app.use( helmet() )
app.use( json() )
app.use( urlencoded({ extended: true }) )

const server = http.createServer(app)

exports.app = app
exports.server = server