const multer = require('multer')

const fileFilter = (req, file, cb) => {
	return cb(null, true)
}

const storage = multer.memoryStorage({
	destination: (req, file, cb) => {
		cb(null, '')
	}
})

module.exports = multer({ storage, fileFilter }).array('files', 20)