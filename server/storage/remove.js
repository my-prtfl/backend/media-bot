const storage = require("./index")

module.exports = async (name, bucket) => {
	console.log("Start removing", name)

	await storage.removeObject(bucket, filenames)

	console.log(name, "successfully removed")
}