const sharp = require("sharp"),
			storage = require("./index")

module.exports = (file, name, bucket = "images") => {
	console.log("Upload started")

	return new Promise(async (resolve, reject) => {
		if (!file) return reject( new Error("No file for upload!") )
		if (!file.buffer) return reject( new Error("No buffer") )

		if (bucket == "images") {
			const buffer = await sharp( file.buffer ).jpeg({ quality: 80 }).toBuffer()
	
			storage.putObject(bucket, `${name}.jpeg`, buffer).catch(reject)
	
			console.log(`${name}.jpeg uploaded`)
			return resolve(`${name}.jpeg`)
		}

		await storage.putObject(bucket, name, file.buffer).catch(reject)

		console.log(`${name} uploaded`)
		return resolve(name)
	})
}