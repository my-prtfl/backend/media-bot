const { Router } = require("express"),
			got = require("got"),
			stream = require("stream"),
			axios = require("axios"),
			minio = require("server/storage")

const router = Router()

router.route("/file/:name")
	// Получаем файл из чата
	.get(async ( req, res ) => {
		try {
			const { id } = req.query
			
			if (!id)
				return res.sendStatus(404)

			const origin = process.env.NODE_ENV == "production" ? "" : "http://localhost:8100"
			res.setHeader("Access-Control-Allow-Origin", origin)
			
			const path = await axios.get(`https://api.telegram.org/bot${process.env.TOKEN}/getFile?file_id=${id}`).then(({ data }) => data.result.file_path)

			got
				.stream(`https://api.telegram.org/file/bot${process.env.TOKEN}/${path}`)
				.on("error", () => res.sendStatus(404))
				.pipe(res)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

router.route("/storage/:bucket/:file")
	// Получаем файлы из хранилища
	.get(async (req, res) => {
		try {
			const { bucket, file } = req.params

			const f = await minio.getObject(bucket, file).catch(() => null)

			if (!f) return res.sendStatus(404)

			const ps = new stream.PassThrough()

			stream.pipeline(f, ps,
				err => {
					if (err) {
						console.error(err)
						res.sendStatus(404)
					}
				}
			)
			
			res.setHeader("Access-Control-Allow-Origin", "*")
			
			ps.pipe(res)
		} catch(err) {
			console.error(err)
			res.sendStatus(404)
		}
	})

router.route("/image.jpeg")
	.get(async ( req, res ) => {
		try {
			const { id } = req.query
			
			if (!id)
				return res.sendStatus(404)

			// const origin = process.env.NODE_ENV == "production" ? "https://bot-crm.media.roybots.ru" : "http://localhost:8100"
			// res.setHeader("Access-Control-Allow-Origin", origin)
			res.setHeader("Access-Control-Allow-Origin", "*")
			
			const path = await axios.get(`https://api.telegram.org/bot${process.env.TOKEN}/getFile?file_id=${id}`).then(({ data }) => data.result.file_path)

			got
				.stream(`https://api.telegram.org/file/bot${process.env.TOKEN}/${path}`)
				.on("error", () => res.sendStatus(404))
				.pipe(res)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router