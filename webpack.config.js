require('dotenv').config()

module.exports = {
	mode: process.env.NODE_ENV,
	devtool: 'cheap-module-source-map',
	output: {
		filename: '[name].chunk.js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|index.js)/,
				loader: 'babel-loader'
			},
			{
        test: /\.css$/i,
        use: [ "style-loader", "css-loader" ]
      }
		]
	}
}