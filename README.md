# Media BOT

> Cryptopizza Media [telegram bot](https://t.me/cryptopizza_bot) with information for partners and chat with manager + API

## Stack
* Telegraf
* Express
* Socket.io
* PostgreSQL
* Sequelize
* [Minio](https://min.io/) (S3 storage)

## Functional:
* Authorization with cookies
* Manipulation with database using Sequelize
* Resizing images using [Sharp](https://www.npmjs.com/package/sharp) and uploading on Minio
* Notifications and chat using socket.io
* Bot logic with [Telegraf.js](https://telegraf.js.org/)
